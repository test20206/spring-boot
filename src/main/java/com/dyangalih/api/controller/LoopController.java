package com.dyangalih.api.controller;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/loop")
public class LoopController {
    @RequestMapping(value = "/{number}", method = RequestMethod.GET)
    public String loop(@PathVariable Integer number) {
        for (int i = 0; i < number; i++) {
            String star = " ".repeat((number - i)) +
                    "*".repeat(i + 1);
            System.out.println(star);
        }
        return "Check Console Log";
    }
}
