package com.dyangalih.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/hello")
public class HelloController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String sayHello(){
        return "Hello";
    }
}
